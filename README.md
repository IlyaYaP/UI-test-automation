# Небольшой проект по автоматизации тестирования UI.
В данном проекте реализована автоматизация тестирования UI с генерацией Allure-отчетов в Gitlab Pages.


[![Python](https://img.shields.io/badge/-Python-464646?style=flat-square&logo=Python)](https://www.python.org/)
[![pytest](https://img.shields.io/badge/-pytest-464646?style=flat-square&logo=pytest)](https://docs.pytest.org/en/stable/contents.html/)
[![Allure](https://img.shields.io/badge/-Allure-464646?style=flat-square&logo=Allure)](http://allure.qatools.ru/)

### Стек технологий:
Стек: Python, Pytest, Selenium, Allure.
- Автоматизация тестирования UI с генерацией Allure-отчетов в Gitlab Pages, используя локальный Gitlab Runner.

# Локальный запуск:
- Клонируем данный репозиторий:
```
git clone https://gitlab.com/IlyaYaP/UI-test-automation.git
```
- В папке с проектом создаем и активируем виртуальное окружение:
```
python -m venv venv
source venv/scripts/activate
```
- Устанавливаем зависимости:
```
python -m pip install --upgrade pip
pip install -r requirements.txt
```
- Устанавливаем allure на Windows(если у вас данная ОС) через scoop.
```
scoop install allure 
```
- Запускаем тесты (Chrome):
```
pytest -s -v --alluredir result_allure --tb=long
```
- Формируем отчет allure.
```
allure serve result_allure
```
